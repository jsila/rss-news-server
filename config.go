package main

type Config struct {
	FeedsFile         string
	LogFile           string
	LimitItemsPerFeed int
	Port              int
	ImagesFolder      string
}

func NewConfig(feedsFile string, logFile string, limitItemsPerFeed int, port int, imagesFolder string) *Config {
	c := Config{
		FeedsFile:         feedsFile,
		LogFile:           logFile,
		LimitItemsPerFeed: limitItemsPerFeed,
		Port:              port,
		ImagesFolder:      imagesFolder,
	}
	return &c
}
