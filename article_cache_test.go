package main

import (
	"fmt"
	"testing"
	"time"
)

func TestArticleCache(t *testing.T) {
	// Not really a test, muahaha

	articleCache, err := NewArticleCache(2 * time.Second)

	if err != nil {
		t.Fatal(err)
	}

	now := time.Now()
	a := &Article{
		Date:        &now,
		Title:       "title",
		Description: "description",
		URL:         "url",
		Domain:      "domain",
		Image:       "image",
	}

	fmt.Println(articleCache.Save(a))
	fmt.Println()
	fmt.Println(articleCache.Get(a.URL))
	fmt.Println()
	fmt.Println(articleCache.Get("undefined"))
	time.Sleep(time.Second * 3)
	fmt.Println()
	fmt.Println(articleCache.Get(a.URL))
	fmt.Println(articleCache.FlushAll().Result())
}
