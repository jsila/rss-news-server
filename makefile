run: build
	./rss-news

build:
	go build -o rss-news

reset:
	rm -rf images
	echo FLUSHALL | redis-cli

deploy:
	systemctl disable rss-news.service
	cp rss-news.service /etc/systemd/system
	systemctl enable rss-news.service
	systemctl start rss-news.service

restart: build
	systemctl restart rss-news.service	