package main

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"
	"time"

	"golang.org/x/net/html"
)

type Article struct {
	ID          string     `json:"id"`
	Title       string     `json:"title"`
	URL         string     `json:"url"`
	Description string     `json:"description"`
	Image       string     `json:"image"`
	Date        *time.Time `json:"date"`
	Domain      string     `json:"domain"`
	Categories  []string   `json:"categories"`
	FeedLink    string
	Site        string `json:"site"`
}

func (a *Article) String() string {
	return fmt.Sprintf("Article '%s' [%s]", a.Title, a.Domain)
}

func (a *Article) FetchImageAndDescription() error {
	resp, err := http.Get(a.URL)
	if err != nil {
		return fmt.Errorf("can't get url: %v", err)
	}
	defer resp.Body.Close()

	doc, err := html.Parse(resp.Body)
	if err != nil {
		return fmt.Errorf("can't parse body: %v", err)
	}

	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "meta" {
			property := ""
			content := ""
			for _, attr := range n.Attr {
				switch attr.Key {
				case "property":
					property = attr.Val
				case "content":
					content = attr.Val
				}
			}
			switch property {
			case "og:description":
				a.Description = content
			case "og:image":
				if strings.HasPrefix(content, "/") && !strings.HasPrefix(content, "//") {
					content = a.FeedLink + content
				}
				a.Image = content
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	return nil
}

func NewArticle(item *Item, imagesFolder string) (*Article, error) {
	a := &Article{
		ID:         Base64Encode(item.Link),
		Site:       item.Site,
		Domain:     item.Domain,
		Date:       item.PublishedParsed,
		Title:      item.Title,
		URL:        item.Link,
		Categories: item.Categories,
		FeedLink:   item.FeedLink,
	}

	if err := a.FetchImageAndDescription(); err != nil {
		return nil, fmt.Errorf("can get image and description for %s: %v", a.URL, err)
	}

	if err := a.StoreImageLocally(imagesFolder); err != nil {
		return nil, fmt.Errorf("can't store image locally: %v", err)
	}

	return a, nil
}

func (a *Article) StoreImageLocally(imagesFolder string) error {
	if err := os.MkdirAll(imagesFolder, os.ModePerm); err != nil {
		return err
	}

	u, err := url.Parse(a.ID + path.Ext(a.Image))
	if err != nil {
		return fmt.Errorf("can't parse image's filename: %v", err)
	}

	if err := SaveImageFromURL(a.Image, u.Path, imagesFolder); err != nil {
		return fmt.Errorf("can't save image: %v", err)
	}

	a.Image = u.Path
	return nil
}

type ArticleCallback func(a *Article, fresh bool) error

func GetArticles(items Items, imagesFolder string, articleCache *ArticleCache, acb ArticleCallback) error {
	articles := []*Article{}
	for _, item := range items {
		a, exists, err := articleCache.Get(item.Link)
		if err != nil {
			return fmt.Errorf("can't see if article is in cache or not: %s", err)
		}
		if !exists {
			a, err = NewArticle(item, imagesFolder)
			if err != nil {
				return fmt.Errorf("can't create new article: %v", err)
			}
			if err := articleCache.Save(a); err != nil {
				return fmt.Errorf("can save article %s to cache: %v", a.URL, err)
			}
		}
		if err = acb(a, !exists); err != nil {
			return fmt.Errorf("can't execute article callback for %s: %v", a.URL, err)
		}
		articles = append(articles, a)
	}

	if err := articleCache.DeleteOutdatedArticles(articles, imagesFolder); err != nil {
		return fmt.Errorf("can't delete outdated urls: %v", err)
	}

	return nil
}
