package main

import (
	"encoding/json"
	"fmt"
	"path"

	"github.com/go-redis/redis"
)

type ArticleCache struct {
	*redis.Client
}

func (r *ArticleCache) Save(a *Article) error {
	articleJSON, err := json.Marshal(a)
	if err != nil {
		return fmt.Errorf("can't convert to json: %v", err)
	}
	if err = r.RPush("urls", a.URL).Err(); err != nil {
		return err
	}
	return r.Set(a.URL, articleJSON, 0).Err()
}

func (r *ArticleCache) Get(url string) (*Article, bool, error) {
	val, err := r.Client.Get(url).Result()
	if err == redis.Nil {
		return nil, false, nil
	} else if err != nil {
		return nil, false, err
	}

	a := &Article{}
	if err = json.Unmarshal([]byte(val), a); err != nil {
		return nil, false, fmt.Errorf("can't get article from map: %v", err)
	}

	return a, true, nil
}

func (r *ArticleCache) DeleteOutdatedArticles(currentArticles []*Article, imagesFolder string) error {
	storedArticles, err := r.GetStoredArticles()
	if err != nil {
		return fmt.Errorf("can't get stored articles: %v", err)
	}

	for _, a := range GetOutdatedArticles(storedArticles, currentArticles) {
		if err = r.LRem("urls", 1, a.URL).Err(); err != nil {
			return fmt.Errorf("can't delete url from list %s: %v", a.URL, err)
		}
		if err = r.Del(a.URL).Err(); err != nil {
			return fmt.Errorf("can't delete url %s: %v", a.URL, err)
		}
		if err := DeleteImage(path.Join(imagesFolder, a.Image)); err != nil {
			return fmt.Errorf("can't delete image: %v", err)
		}
	}

	return nil
}

func (r *ArticleCache) GetStoredArticles() ([]*Article, error) {
	storedURLs, err := r.LRange("urls", 0, -1).Result()
	if err != nil {
		return nil, fmt.Errorf("can't get urls from redis cache: %v", err)
	}
	storedArticles := []*Article{}
	for _, u := range storedURLs {
		a, _, err := r.Get(u)
		if err != nil {
			return nil, fmt.Errorf("can't get article from cache: %v", err)
		}
		storedArticles = append(storedArticles, a)
	}
	return storedArticles, nil
}

func NewArticleCache() (*ArticleCache, error) {
	redisClient := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	_, err := redisClient.Ping().Result()
	if err != nil {
		return nil, fmt.Errorf("can't ping redis server: %v", err)
	}

	a := &ArticleCache{
		Client: redisClient,
	}

	return a, nil
}

// GetOutdatedArticles returns stored articles (in first collection) that aren't in current (second collection)
func GetOutdatedArticles(stored []*Article, current []*Article) []*Article {
	res := []*Article{}
STORED:
	for _, s := range stored {
		for _, c := range current {
			if s.URL == c.URL {
				continue STORED
			}
		}
		res = append(res, s)
	}
	return res
}
