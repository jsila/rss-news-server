# Project description

RSS News reads RSS feeds of selected news sites and for each item visits its url and scrapes page's description and image (from opengraph meta tags). Items are cached in Redis (item's title, description, link,...) and server's filesystem (item's image), so item is scraped from original source only once. After each visit outdated items are removed due to new articles being published. When item is being scraped, it's tagged as fresh so it's highlighted when displayed in the browser. Back-end is written in Go.

Frontend is written in React+Redux. It uses websockets to query items. Items are showed in magazine style with image, title, description, domain source and tags. Images are lazy-loaded (they are loaded when user scrolls to it).

Tools used: Go, React+Redux (bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).), Websockets, Redis.

Front-end code can be found in [seperate repository](https://bitbucket.org/jsila/rss-news-site) on Bitbucket.