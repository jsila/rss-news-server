package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/websocket"
)

func GetLogger(file string) (*log.Logger, error) {
	f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		return nil, fmt.Errorf("can't open file: %v", err)
	}
	return log.New(f, "", log.Lshortfile|log.LstdFlags), nil
}

func Abort(appLog *log.Logger, msg string, v ...interface{}) {
	appLog.Printf(msg, v...)
	log.Fatalf(msg, v...)
	os.Exit(1)
}

func main() {
	feedsFile := flag.String("feeds", "./feeds.yml", "file path to yaml file of list of feeds")
	logFile := flag.String("log", "./log.txt", "file path to log file")
	limitItemsPerFeed := flag.Int("limit", 10, "number of articles per feed")
	imagesFolder := flag.String("imagesFolder", "./images", "path to folder with articles' image")
	port := flag.Int("port", 8080, "port to listen to")

	flag.Parse()

	c := NewConfig(*feedsFile, *logFile, *limitItemsPerFeed, *port, *imagesFolder)

	appLog, err := GetLogger(c.LogFile)
	if err != nil {
		Abort(appLog, "can't get logger: %v", err)
	}

	articleCache, err := NewArticleCache()
	if err != nil {
		Abort(appLog, "can't create new articles cache: %v", err)
	}

	feeds, err := GetFeeds(c.FeedsFile)
	if err != nil {
		Abort(appLog, "can't get feeds: %v", err)
	}

	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			appLog.Printf("access from %s\n", r.Header.Get("Origin"))
			return true
		},
	}

	http.Handle("/images/", http.StripPrefix("/images/", http.FileServer(http.Dir(c.ImagesFolder))))

	http.HandleFunc("/articles", func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			appLog.Println(err)
			return
		}

		items, err := GetItems(feeds, c.LimitItemsPerFeed)
		if err != nil {
			appLog.Println(err)
			return
		}

		err = GetArticles(items, c.ImagesFolder, articleCache, func(a *Article, fresh bool) error {
			aa := struct {
				*Article
				Fresh bool `json:"fresh"`
			}{
				Article: a,
				Fresh:   fresh,
			}

			return conn.WriteJSON(aa)
		})
		if err != nil {
			appLog.Printf("can't get articles: %v\n", err)
		}
	})

	fmt.Printf("Listening on port %d\n", c.Port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", c.Port), nil))
}
