package main

import (
	"encoding/base64"
	"io"
	"net/http"
	"os"
	"path"
)

func Base64Encode(text string) string {
	return base64.StdEncoding.EncodeToString([]byte(text))
}

func Base64Decode(text string) (string, error) {
	decoded, err := base64.StdEncoding.DecodeString(text)
	if err != nil {
		return "", err
	}
	return string(decoded), nil
}

func SaveImageFromURL(url string, filename string, folder string) error {
	if url == "" {
		return nil
	}
	newPath := path.Join(folder, filename)
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	file, err := os.Create(newPath)
	if err != nil {
		return err
	}
	defer file.Close()

	if _, err := io.Copy(file, resp.Body); err != nil {
		return err
	}
	return nil
}

func DeleteImage(path string) error {
	return os.Remove(path)
}
