package main

import (
	"io/ioutil"
	"net/url"
	"strings"

	"github.com/mmcdole/gofeed"

	"gopkg.in/yaml.v2"
)

type Feed struct {
	Site string `yaml:"site"`
	Link string `yaml:"link"`
	RSS  string `yaml:"rss"`
	Feed *gofeed.Feed
}

type Feeds []*Feed

func (f *Feed) Domain() (string, error) {
	parsed, err := url.Parse(f.Link)
	if err != nil {
		return "", err
	}
	return strings.TrimPrefix(parsed.Host, "www."), nil
}

func GetFeeds(filename string) (Feeds, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var feeds Feeds
	if err := yaml.Unmarshal(b, &feeds); err != nil {
		return nil, err
	}

	return feeds, nil
}
