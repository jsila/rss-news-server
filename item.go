package main

import (
	"fmt"
	"sort"

	"github.com/mmcdole/gofeed"
)

type Item struct {
	*gofeed.Item
	Domain   string
	FeedLink string
	Site     string
}

type Items []*Item

func GetItems(feeds Feeds, limit int) (Items, error) {
	items := Items{}
	var err error

	fp := gofeed.NewParser()
	for _, feed := range feeds {
		if feed.Feed, err = fp.ParseURL(feed.RSS); err != nil {
			return nil, err
		}

		domain, err := feed.Domain()
		if err != nil {
			return nil, fmt.Errorf("can't get link's domain: %v", err)
		}

		for i, item := range feed.Feed.Items {
			if i >= limit {
				break
			}
			items = append(items, &Item{
				Item:     item,
				Domain:   domain,
				FeedLink: feed.Link,
				Site:     feed.Site,
			})
		}
	}
	sort.Slice(items, func(i, j int) bool {
		return (*items[i].PublishedParsed).After(*items[j].PublishedParsed)
	})
	return items, nil
}
